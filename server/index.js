const express = require('express')
const dotenv = require('dotenv')
const bodyParser = require('body-parser')
const app = express()
const mongoose = require('mongoose')
const userRoutes = require('./routes/User')

dotenv.config()
app.use(express.json())
app.use(express.urlencoded({ extended: true }));
app.use('/user', userRoutes)
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.get('/', (req, res) => {
    res.send('home page')
})

const connect = async () => await mongoose.connect(
    process.env.MONGO_URL, 
    {
    useNewUrlParser: true,
    // useCreateIndex: true,
    useUnifiedTopology: true
}).then(
    () => console.log('successfully established the connections')).catch((error) => console.log(error.message))

connect()
app.listen(process.env.PORT, () => console.log('server running on 5000'))
