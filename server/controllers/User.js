const express = require('express')
const mongoose = require('mongoose')
const bcrypt = require('bycryptjs')
const User = require('../models/UserModel.js')



const show_users = async (req, res) => {
    // console.log('entered show users')
   try {
         const users = await User.find({})
         console.log(users)
         users.forEach((user) => {
             `<h1>{user}</h1>`
         })
         res.status(200).send(users)
   } catch (error) {
       res.status(404).json({message: error.message})
   }
}

const create_user = async (req, res) => {
   try {
       const {name, password} = req.body
       pass = await bcrypt.hash(password, 10);
    console.log(req.body)
       var array = [{
           name:name,
           password: pass
       }];
       User.create(
           array
       ).then((docs) => {
           docs.forEach(element => {
               element.save()
           });
            // console.log(docs)
           
       });
    //    await array.save()
       res.send(array)
   } catch (error) {
       res.status(404).send(error)
   }
}
module.exports = { show_users, create_user }