const express = require('express')
const router = express.Router()

const { show_users, create_user }  = require('../controllers/User')


router.get('/', (req, res) => {
    res.send("router")
    console.log('routes home page')
})

router.get('/show_users', show_users);

router.post('/create_user', create_user)
module.exports = router;
