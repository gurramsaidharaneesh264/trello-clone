import React from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import '../styles/Login.css'
function Login() {
    return (
        <div className="input-main">
            <h1>Trello Login</h1>
            <Form className="input-form">
                <Form.Group className="mb-3 group" controlId="formBasicEmail">
                    <Form.Label className="label">Email address</Form.Label>
                    <Form.Control className="input" type="email" placeholder="Enter email" />
                </Form.Group>

                <Form.Group className="mb-3 group" controlId="formBasicPassword">
                    <Form.Label className="label">Password</Form.Label>
                    <Form.Control className="input" type="password" placeholder="Password" />
                </Form.Group>
                <Button className="submit-btn"variant="secondary" type="submit">
                    Submit
                </Button>
            </Form>
        </div>
    )
}

export default Login
